package com.netlink.cicd.CICD.Demo.controller;

import com.netlink.cicd.CICD.Demo.model.Employee;
import com.netlink.cicd.CICD.Demo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public Employee saveEmployee(@RequestBody Employee employee) {
        return employeeService.saveEmployee(employee);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Employee> getEmployees() {
        return employeeService.getEmployees();
    }

    @DeleteMapping(value = "/{employee-id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public Employee deleteEmployee(
            @PathVariable(name = "employee-id") Integer employeeId) {
        return employeeService.deleteEmployee(employeeId);
    }

    @PatchMapping(value = "/{employee-id}")
    @ResponseStatus(HttpStatus.OK)
    public Employee updateEmployee(@PathVariable(name = "employee-id") Integer employeeId,
                                   @RequestBody Employee employee) {
        return employeeService.updateEmployee(employeeId, employee);
    }
}
